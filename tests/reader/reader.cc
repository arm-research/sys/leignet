#include <iostream>
#include <cstdlib>
#include <memory>
#include <string>

#include <buf/buf.h>
#include "schema.h"


int main(int argc, const char** argv) {
    if (argc != 2) {
        std::cerr << "usage: " << argv[0] << " <file name>" << std::endl;
        exit(1);
    }

    ipc::buffer::register_signal_handlers();

    auto bus = new buf::SimLink(std::string(argv[1]));
    auto c = bus->consumer(0);

    int i = 0;
    while (true) {
        auto kind = c->open();
        std::cout << "[" << i << "] ";
        switch(kind) {
            case Ins::kind:
                std::cout << std::hex << Ins::pc(c) << " : " << Ins::bits(c) << std::endl;
                break;
            case MemRead8::kind:
                std::cout << std::hex << "    Mem8[" << MemRead8::addr(c) << "] => " << MemRead8::data(c) << std::endl;
                break;
            case MemRead16::kind:
                std::cout << std::hex << "    Mem16[" << MemRead16::addr(c) << "] => " << MemRead16::data(c) << std::endl;
                break;
            case MemRead32::kind:
                std::cout << std::hex << "    Mem32[" << MemRead8::addr(c) << "] => " << MemRead32::data(c) << std::endl;
                break;
            case MemRead64::kind:
                std::cout << std::hex << "    Mem8[" << MemRead64::addr(c) << "] => " << MemRead64::data(c) << std::endl;
                break;
            default:
                std::cerr << "unknown kind " << kind << std::endl;
                exit(1);
        }
        c->close();
        i++;
    }

    return 0;
}