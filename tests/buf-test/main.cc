#include <string>
#include <buf/buf.h>
#include <memory>
#include <thread>
#include <csignal>
#include <unistd.h>

constexpr int N = 1000000000;

volatile int p_index = 0;
volatile int c_index = 0;

void handler(int) {
    printf("P:%d C:%d\n",p_index,c_index);    
}

void shm_reset(const char *handle) {
    char *fullpath = (char *) malloc(strlen(handle)+10);
    shm_unlink(handle);
    snprintf(fullpath, strlen(handle)+10, "/dev/shm/%s", handle);
    remove(fullpath);
    free(fullpath);
}

int main() {

    ipc::buffer::register_signal_handlers();

    std::string file("leignet.test.buf");
    shm_reset("leignet.test.buf");

    auto link = new buf::SimLink(file);

    signal(SIGINT,handler);

    auto child = std::thread([file, link] {
        auto c = link->consumer(0);

        for (c_index=0; c_index<N; c_index++) {
            c->open([c] {
                auto v = c->get<int>(0);
                if (v != c_index) {
                    printf("consumer: expected %d, found %d\n",c_index,v);
                    exit(-1);
                }
            });
        }
        delete c;
    });

    auto p = link->producer(0);
    for (p_index=0; p_index<N; p_index++) {
        p->open(4,42,[p] {
            p->put<int>(p_index,0);
        });
    }

    child.join();
    delete p;
    delete link;
    return 0;
}

