#include <string>
#include <buf/buf.h>
#include <memory>
#include <thread>
#include <csignal>
#include <unistd.h>
#include "schema.h"
#include <numeric>

bool debug = std::getenv("DEBUG")!=NULL;
bool trace = std::getenv("TRACE")!=NULL;

#define DEBUG(x) if(debug) x
#define TRACE(x) if(trace) x

constexpr int N = 1000;  /* number of clocks to run for */

// Producer Code
void prodthread(buf::SimLink *link, uint16_t chan, uint64_t quanta)
{
    auto p = link->producer(chan);
    auto c = link->consumer(-chan);

    uint64_t local_clock = 1;
    DEBUG( std::cout << "Producer Thread: " << chan << " Quanta: " << quanta << std::endl );

    // send our minimum clock quanta
    p->open<ClockPeriod>();
    ClockPeriod::clock(p, local_clock);
    ClockPeriod::quanta(p, quanta);
    p->close();

    while (local_clock < N)
    {
        int qc = 0;

        // receive our clock allocation
        auto kind = c->open();

        if (kind != ClockPeriod::kind)
        {
            std::cerr << "ERROR: Producer: unexpected message from hub " << kind << std::endl;
            p->open<Exit>();
            Exit::exit_code(p, -1);
            p->close();
            c->close();
            delete c;
            delete p;
            exit(-1);
        }
        qc = ClockPeriod::quanta(c);
        if(ClockPeriod::clock(c) < local_clock)
            std::cerr << "*** ERROR *** Turning back time local " << local_clock << " sync " << ClockPeriod::clock(c) << std::endl;
        local_clock = ClockPeriod::clock(c);

        c->close();

        for (int q = 0; q < qc; q++)
        {
            p->open<Ins>();
            Ins::clock(p, local_clock);
            p->close();
            if(++local_clock > N)
                break;
        }
    }

    DEBUG( std::cout << "Producer: Cleaning up" << std::endl );
    p->open<Exit>();
    Exit::exit_code(p, 0);
    p->close();
    delete c;
    delete p;
}

class SubSim {
public:
    uint16_t channel;
    uint64_t quanta = 0;
    uint64_t clock = 1;
    uint64_t epoc = 0;
    int code = 1;

    buf::Producer *p;
    buf::Consumer *c;
    std::thread t;

    explicit SubSim(buf::SimLink *l, int16_t c, uint64_t q);
    virtual ~SubSim();
};

SubSim::SubSim(buf::SimLink *l, int16_t chan, uint64_t q)
{
    channel = chan;
    p = l->producer(-chan);
    c = l->consumer(chan);
    t = std::thread(prodthread, l, chan, q);
}

SubSim::~SubSim()
{
    channel = 0;

    t.join();

    delete p;
    delete c;
}

uint64_t
calculate_quanta( std::vector<SubSim *> &sims ) 
{
    uint64_t lcm_quanta = 1;

    for(long unsigned int i=0; i < sims.size(); i++) {
        lcm_quanta = std::lcm(lcm_quanta, sims[i]->quanta);
    }

    return lcm_quanta;
}


int main(int argc, char **argv)
{
    int num_threads=argc-1;
    uint64_t epoc_quanta = 0;
    uint64_t next_quanta = 0;

    if(argc < 2) {
        std::cerr << "clock-test <quanta #1> [quanta #2] ... [quants #n]" << std::endl;
        exit(-1);
    }

    // register ipc clean-up in case of death
    ipc::buffer::register_signal_handlers();

    std::string file("leignet.test.clock-sync");
    auto link = new buf::SimLink(file);

    std::vector<SubSim *> sims;
    sims.reserve(num_threads+1);

    for(int simnum = 1; simnum < num_threads+1; simnum++ )
        sims.emplace_back(new SubSim(link, simnum, std::stoull(argv[simnum])));

    uint64_t clock = 1;
    uint64_t epoc = 1;
    int active_sim = 0;
    int active_sims = num_threads;

    while(active_sims)
    {
        auto kind = sims[active_sim]->c->poll();
        if(!kind) { // channel is empty
            active_sim = (active_sim+1) % num_threads; /* round robin */
            std::this_thread::yield();
            if ((sims[active_sim]->quanta) && (sims[active_sim]->epoc < epoc) && (sims[active_sim]->quanta <= epoc_quanta))
            {
                DEBUG( std::cout << "Consumer: Releasing producer " << active_sim << " (" << sims[active_sim]->quanta <<") for 0x" << epoc_quanta << " @" << (epoc-epoc_quanta) << std::endl );
                sims[active_sim]->epoc = epoc;
                sims[active_sim]->p->open<ClockPeriod>();
                ClockPeriod::clock(sims[active_sim]->p, epoc-epoc_quanta);
                ClockPeriod::quanta(sims[active_sim]->p, epoc_quanta);
                sims[active_sim]->p->close();
            }
            continue;
        }
        TRACE( std::cout << "(" << active_sim << ") [" << kind << "] " );
        switch (kind)
        {
        case Ins::kind:
            sims[active_sim]->clock = Ins::clock(sims[active_sim]->c);
            if(sims[active_sim]->clock > clock)
                clock = sims[active_sim]->clock;
            TRACE( std::cout << std::hex << epoc << ":" << clock << " : " << sims[active_sim]->clock << std::endl );
            if(clock > epoc) { /* sanity check */
                std::cerr << "*** ERROR *** producer " << active_sim << " observed clock " << clock << " is greater than epoc " << epoc << std::endl;
            }
            break;
        case ClockPeriod::kind:
            TRACE( std::cout << std::hex << sims[active_sim]->epoc << " : " << ClockPeriod::quanta(sims[active_sim]->c) << std::endl );
            if (ClockPeriod::quanta(sims[active_sim]->c) != sims[active_sim]->quanta)
            {
                sims[active_sim]->quanta = ClockPeriod::quanta(sims[active_sim]->c);
                DEBUG( std::cout << "Consumer: New producer " << active_sim << " minimum quanta 0x" << sims[active_sim]->quanta << std::endl );
                next_quanta = calculate_quanta( sims );

                if(next_quanta != epoc_quanta)
                    DEBUG( std::cout << "Consumer: next_quanta now " << next_quanta << std::endl );
            }
            break;
        case Exit::kind:
            sims[active_sim]->code = Exit::exit_code(sims[active_sim]->c);
            active_sims--;
            TRACE( std::cout << std::endl );
            DEBUG( std::cout << "Exit " << sims[active_sim]->code << " number of sims remaining: " << active_sims << std::endl );
            sims[active_sim]->clock = -1;
            sims[active_sim]->epoc = -1;
            break;
        default:
            std::cerr << "unknown kind " << kind << std::endl;
            exit(1);
        }
        sims[active_sim]->c->close();

        /* check to see if current sim has moved passed the current epoc */
        int rr = 0;
        int s = active_sim;
        for(int count = 0; count < num_threads; count++) {
            s = (s + count) % num_threads;
            if((!sims[s]->code) || (sims[active_sim]->quanta > epoc_quanta))
                continue;
            if(sims[s]->clock+1 < epoc) {
                rr = 1;
                break;
            }
        }
        if(rr)
            active_sim = s;
        else if(active_sims) {
            if(clock+1 >= epoc) {
                if(next_quanta != epoc_quanta) {
                    DEBUG( std::cout << "consuemr: setting old epoc quanta " << epoc_quanta << " to next " << next_quanta << std::endl );
                    epoc_quanta = next_quanta;
                }
                epoc += epoc_quanta;
                DEBUG( std::cout << "Consumer: Advancing epoc to " << epoc << std::endl );
                active_sim = (active_sim+1) % num_threads; /* round robin */
            }
        }
    }

    DEBUG( std::cout << "Consumer: Cleaning up" << std::endl );
    for(long unsigned int i=0; i < sims.size(); ++i) {
        delete sims[i];
    }

    DEBUG( std::cout << "Consumer: Shutting down link" << std::endl );
    delete link;

    std::cout << "DONE: Executed " << clock << " cycles of simulation across " << num_threads << " threads." << std::endl;
    
    return 0;
}
