default_target : all;

BUILD_NAME ?= ${shell uname}
BUILD_DIR=build/${BUILD_NAME}
INSTALL_PREFIX ?= /usr/local

CMAKE=cmake

sdl/target/universal/stage/bin/sdl :
	(cd sdl; make all)

schema.h: sdl/target/universal/stage/bin/sdl schema.sdl
	rm -f schema.h
	sdl/target/universal/stage/bin/sdl schema.sdl

${BUILD_DIR} : 
	${CMAKE} -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -DCMAKE_BUILD_TYPE=Release -S . -B ${BUILD_DIR}/release
	${CMAKE} -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -DCMAKE_BUILD_TYPE=Debug -S . -B ${BUILD_DIR}/debug

all : % : ${BUILD_DIR} schema.h
	(cd ${BUILD_DIR}/debug; make $@)
	(cd ${BUILD_DIR}/release; make $@)

clean :
	rm -f schema.h
	(cd sdl; make clean)
	rm -rf build

debug : ${BUILD_DIR} schema.h
	(cd ${BUILD_DIR}/debug; make)

release : ${BUILD_DIR} schema.h
	(cd ${BUILD_DIR}/release; make)

install: ${BUILD_DIR} schema.h
	(cd ${BUILD_DIR}/release; make install)
