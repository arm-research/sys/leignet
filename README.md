# LeigNet

Proof of concept simulation bridge.  

# Build

## Build Requirements

* make
* cmake (>= 3.13)
* java (>= 8)
* c++17 compatible compiler (should work with both clang and g++)

# Acknowledgements

Original code written by Jonathan Beard and Ahmed Gheith.  Portions of this
work funded by the Department of Energy under Contract Agreement 1966673.

