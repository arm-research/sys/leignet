#ifndef _BUF_H_
#define _BUF_H_

#include <string>
#include <cstdint>
#include <atomic>
#include <unistd.h>
#include <cassert>
#include <sys/mman.h>
#include <buffer>

namespace buf {

struct MsgHeader {
    uint32_t msg_size;
    uint32_t kind;
};

class Producer {
    ipc::thread_local_data* tls_producer;
    ipc::channel_id_t channel_id;
    const uint32_t header_offset = sizeof(struct MsgHeader);
    uint32_t data_size;
    uint8_t *buffer;

public:
    Producer(ipc::buffer *buffer, const ipc::channel_id_t id);
    virtual ~Producer();

    void open(uint32_t sz, uint32_t kind);

    void close();

    template <typename T>
    void open() {
        open(T::total_size,T::kind);
    }

    template <typename W>
    inline void open(uint32_t sz, uint32_t kind, W work) {
        open(sz,kind);
        work();
        close();
    }

    template <typename T>
    inline void put(T t, uint32_t offset) {
        T* ptr = (T*) (buffer+header_offset+offset);
        *ptr = t;       
    }

};
class Consumer {
    ipc::thread_local_data* tls_consumer;
    ipc::channel_id_t channel_id;
    const uint32_t header_offset = sizeof(struct MsgHeader);
    uint32_t data_size;
    uint8_t *buffer = nullptr;

public:
    explicit Consumer(ipc::buffer *buffer, const ipc::channel_id_t id);
    virtual ~Consumer();

    uint32_t open();
    uint32_t poll();

    void close();

    template <typename W>
    inline void open(W work) {
        open();
        work();
        close();
    }

    template <typename T>
    inline T get(uint32_t offset) {
        T* ptr = (T*) (buffer+header_offset+offset);
        return *ptr;
    }
};
class SimLink {
    uint32_t chunk_size;
    ipc::buffer* buffers;
    std::string myhandle;

public:
    explicit SimLink(const std::string &file_path);
    ~SimLink();

    Producer *producer(int16_t channel);
    Consumer *consumer(int16_t channel);
};

}

#endif
