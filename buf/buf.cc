#include "buf.h"

#include <memory>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <cinttypes>
#include <cassert>
#include <iostream>

using namespace buf;

/************ Producer **************/

Producer::Producer(ipc::buffer *buffer, const ipc::channel_id_t id) {
    const auto thread_id    = getpid();
    tls_producer      = ipc::buffer::get_tls_structure( buffer, thread_id );
    channel_id = id;

    if( ipc::buffer::add_spsc_lf_record_channel( tls_producer, channel_id ) == ipc::channel_err )
    {
        return;
    }
}

Producer::~Producer() {
}

void Producer::open(uint32_t sz, uint32_t kind) {
    buffer = (uint8_t *) ipc::buffer::allocate_record( tls_producer, sz, channel_id );
    /* check size */
    ((struct MsgHeader *)buffer)->msg_size = sz + sizeof(struct MsgHeader);
    ((struct MsgHeader *)buffer)->kind = kind;
}

void Producer::close() {
    while( ipc::buffer::send_record( tls_producer, channel_id, (void**)&buffer ) != ipc::tx_success )
            sleep(0);
}

/*************** Consumer *******************/

Consumer::Consumer(ipc::buffer *buffer, const ipc::channel_id_t id) {
    const auto thread_id    = getpid();
    tls_consumer      = ipc::buffer::get_tls_structure( buffer, thread_id );
    channel_id = id;


    if( ipc::buffer::add_spsc_lf_record_channel( tls_consumer, id ) == ipc::channel_err )
    {
        return;
    }
}

Consumer::~Consumer() {

}

uint32_t Consumer::open() {
    while( ipc::buffer::receive_record( tls_consumer, channel_id, (void **) &buffer ) != ipc::tx_success )
        sleep(0);

    return ((struct MsgHeader *)buffer)->kind;
}

uint32_t Consumer::poll() {
    if( ipc::buffer::receive_record( tls_consumer, channel_id, (void **) &buffer ) == ipc::tx_success ) {
            return ((struct MsgHeader *)buffer)->kind;
    } else {
            return 0;
    }
}

void Consumer::close() {
    ipc::buffer::free_record( tls_consumer, buffer );
}

/*************** Consumer *******************/

SimLink::SimLink(const std::string &handle) {
    myhandle = handle;
    buffers = ipc::buffer::initialize(handle);
}

SimLink::~SimLink() {
    ipc::buffer::destruct( buffers, myhandle, true );
}

Producer* SimLink::producer(int16_t index) {
    return new Producer(buffers, index);
}

Consumer* SimLink::consumer(int16_t index) {
    return new Consumer(buffers, index);    
}
