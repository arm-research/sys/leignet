package sdl

import fastparse._
import org.scalatest._
import matchers._
import org.scalatest.propspec.AnyPropSpec
import prop._
//import org.scalatest.prop.TableDrivenPropertyChecks
//import org.scalatest.propspec.AnyPropSpec

import Grammar._

class t1 extends AnyPropSpec with TableDrivenPropertyChecks with should.Matchers {

  val shouldPass: TableFor3[String, P[_] => P[_], _] = Table(
    ("input","rule","output"),
    ("0",binary_digit(_) ,"0"),
    ("1",binary_digit(_),"1"),
    ("struct x {}",struct(_),Struct("x",Seq())),
    (
      """
        |struct x {
        |    int x;
        |    float y;
        |    }
        |""".stripMargin,
      struct(_),
      Struct("x",Seq(Member("int","x"),Member("float","y")))
    )
  )

  val shouldFail : TableFor2[String, P[_] => P[_]] = Table(
    ("input","rule"),
    ("2",binary_digit(_)),
    ("01",binary_digit(_)),
    ("structx {}",struct(_))

  )

  property("should succeed") {
    forAll(shouldPass) { case (in,rule,out) =>
      parseAll(in,rule) should be (out)
    }
  }

  property("should fail") {
    forAll(shouldFail) { case(in,rule) =>
      intercept[GrammarException] {
        parseAll(in,rule)
      }
    }
  }

  property("things") {
    val s = "struct abc { int x; float y; }"
    val n = parseAll(s,struct(_))
    println(n)
    //println(n.genString())
  }


}
