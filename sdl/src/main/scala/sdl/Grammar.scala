package sdl

import fastparse._
import ScalaWhitespace._

object Grammar {

  val keywords: Set[String] = Set("struct","union")

  def id_start[_:P]: P[String] = P {
    CharIn("a-zA-Z_").!
  }

  def id_char[_:P]: P[String] = P { id_start | CharIn("0-9").! }

  def id[_:P]: P[String] = P {
    id_start ~~ id_char.repX() ~~ !(id_char)
  }.map{ case(c,rest) => (c +: rest).mkString }.filter(s => !keywords.contains(s))

  def kw[_:P](s: String): P[Unit] = P(s ~~ !(id_char))

  def binary_digit[_:P]: P[String] = CharIn("01").!

  def decimal_digit[_:P]: P[String] = CharIn("0-9").!

  def hex_digit[_:P]: P[String] = P {
    decimal_digit | CharIn("a-fA-F").!
  }

  def decimal_integer[_:P]: P[BigInt] =
    decimal_digit.
      repX(1).
      map(digits => BigInt(digits.mkString,10))


  def hex_integer[_:P]: P[BigInt] = P {
    "0" ~~ CharIn("xX") ~~ hex_digit.repX(1)
  }.map(digits => BigInt(digits.mkString,16))

  def literal[_:P]: P[BigInt] = P { decimal_integer | hex_integer }

  def member[_:P]: P[Member] = P {
    id ~ id ~ ";"
  }.map { case(t,n) =>
    Member(t,n)
  }

  def struct[_:P]: P[Struct] = P {
    kw("struct") ~ id ~ "{" ~ member.rep(0) ~ "}" ~ "=" ~ decimal_integer
  }.map { case (id,members,n) =>
    Struct(id,members,n)
  }

  def schema[_:P]: P[Schema] = P {
    struct.rep(0)
  }.map { structs =>
    Schema(structs)
  }

  def all[A,_:P](rule: P[_] => P[A]): P[A] = P {
    Start ~ rule(implicitly[P[_]]) ~ End
  }

  def parseAll[A,T](s: String, rule: P[_] => P[A]): A = parse(s,all(rule)(_)) match {
    case s: Parsed.Success[A] => s.value
    case f: Parsed.Failure => throw GrammarException(f)
  }

  def parseAll[A,T](g : geny.Readable, rule: P[_] => P[A]): A = parse(g,all(rule)(_)) match {
    case s: Parsed.Success[A] => s.value
    case f: Parsed.Failure => throw GrammarException(f)
  }

}