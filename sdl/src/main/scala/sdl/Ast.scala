package sdl

import fastparse.Parsed


sealed trait Node {

}


case class Member(typeName: String, memberName: String) extends Node {

}

case class Struct(structName: String, members: Seq[Member], id: BigInt) extends Node {



}

case class Schema(structs: Seq[Struct]) extends Node {

}

case class GrammarException(f: Parsed.Failure) extends Exception {

  override def getMessage: String = f.extra.trace().aggregateMsg

}
