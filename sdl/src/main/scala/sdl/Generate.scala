package sdl

import fmt._

object Generate {


  def apply(member: Member): Fmt = {
    import member._
    fmt"""|
          |$typeName $memberName;"""
  }

  def apply(struct: Struct): Fmt = {
    import struct._
    // set offsets
    //val sm = Stream.fromIterable(members)
    val offsets = "0" +: members.map(m => s"${m.memberName}_offset + sizeof(${m.typeName})")
    val names = members.map(m => s"${m.memberName}_offset") :+ "total_size"
    val set_offsets = Fmt(offsets.zip(names).map { case (offset,name) =>
      fmt"""|constexpr static int $name = $offset;
           |"""
    })

    // Writer

    //val open = gen"Shared<Writer> open(Shared<Producer> p) { p->open(size); return std::make_shared<Writer>(p); }"

    val member_rw = members.map { m =>
      import m._
      fmt"""|static void $memberName(buf::Producer *p, ${typeName} v) { p->put(v,${memberName}_offset); }
            |static ${typeName} $memberName(buf::Consumer *c) { return c->get<$typeName>(${memberName}_offset); }
            |"""
    }

    fmt"""
         |struct $structName {
         |    constexpr static uint32_t kind = ${struct.id.toInt};
         |    $set_offsets
         |    $member_rw
         |};
         |"""
  }

  def apply(schema: Schema): Fmt = {
    fmt"""
      |#include <buf/buf.h>
      |${schema.structs.map(s => apply(s))}
    """
  }

}
