package sdl

import zio._

import scala.collection.Iterable
import scala.util.control.NonFatal

case class Args(files: Seq[String] = Seq()) {
  def parse(args: Seq[String]): Args = {
    if (args.isEmpty) {
      this
    } else {
      this.copy(files = files :+ args.head).parse(args.tail)
    }
  }
}

object Args {
  def apply(in: Iterable[String]): Args =
    Args().parse(in.toSeq)
}

object  main  {

  def one(f: String) : ZManaged[Any,Throwable,Unit] = {
    val path = os.Path(os.FilePath(f),os.pwd)
    val base = path.baseName
    val out = os.pwd / s"$base.h"
    println(s"reading from $path")

    val out_f = Task {

      val schema = Grammar.parseAll(os.read(path),Grammar.schema(_))
      Generate(schema)
    }

    for {
      f <- ZManaged.fromEffect(out_f)
      x <- f.write(out,2,Some('|'))
    } yield x
  }

  def main(args: Array[String]) : Unit = {
    val a = Args(args.toSeq)
    val runtime = zio.Runtime.default

    val work = a.files.map(one).fold(ZManaged.unit)(_ andThen _).use { _ =>
      Task{}
    }

    try {
      runtime.unsafeRunTask(work)
    } catch {
      case NonFatal(e) =>
        println(e)
    }
  }





}