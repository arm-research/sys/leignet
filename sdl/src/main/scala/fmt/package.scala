import zio.stream.UStream

package object fmt {

  implicit class StringHelper(val sc: StringContext) extends AnyVal {
    def fmt(args: Any*): Fmt = {
      Fmt(UStream.fromIterable(sc.parts).interleave(UStream.fromIterable(args)))
    }
  }

}
