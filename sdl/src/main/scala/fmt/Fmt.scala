package fmt

import geny.Writable
import zio.stream._
import zio._

import java.io.{InputStream, OutputStream}
import scala.collection.mutable



trait Fmt {
  import Fmt._

  def format(state: State): Task[State]

  def format(indentWidth: Int, mark: Option[Char]): Task[UStream[Char]] =
    format(State.initial(indentWidth,mark)).map(_.out)

  def indent : Fmt =
    ForIndent(this)

  def toCharIterator(indentWidth: Int, mark: Option[Char]): ZManaged[Any,Throwable,Iterator[Char]] =
    for {
      char_stream <- ZManaged.fromEffect(format(indentWidth, mark))
      char_iterator <- char_stream.toIterator//.map(_.map(_.getOrElse(???)))
    } yield char_iterator.map(_.getOrElse(???))


  def toInputStream(indentWidth: Int, mark: Option[Char]): ZManaged[Any,Throwable,InputStream] = {
    for {
      char_stream <- ZManaged.fromEffect(format(indentWidth, mark))
      byte_stream <- char_stream.map(_.toByte).toInputStream // TODO: unicode
    } yield byte_stream
  }

  def toWritable(indentWidth: Int, mark: Option[Char]): ZManaged[Any,Throwable,geny.Writable] =
    for {
      input_stream <- toInputStream(indentWidth,mark)
    } yield new Writable {
      override def writeBytesTo(out: OutputStream): Unit = input_stream.transferTo(out)
    }

  def write(path: os.Path, indentWidth: Int, mark: Option[Char]): ZManaged[Any,Throwable,Unit] =
    for {
      w <- toWritable(indentWidth,mark)
    } yield {
      println(s"write to $path")
      os.write.over(path,w)
    }

  /*
  def toSource(indentWidth: Int, mark: Option[Char]): Task[os.Source] = for {
    w <- toWritable(indentWidth,mark)
  } yield new os.Source {
    override def getHandle(): Either[geny.Writable, SeekableByteChannel] = Left(w)
  }
   */

}

object Fmt {

  case class Fixed(
                  mark: Option[Char],
                  indentWidth: Int
                  )

  case class State(
                    fixed: Fixed,
                    startOfLine: Boolean,
                    mySpaces: Int,
                    pre: UStream[Char], // includes the starting newline
                    out: UStream[Char]
                  )

  object State {
    def initial(indentWidth: Int, mark: Option[Char]): State = State(
      fixed = Fixed(mark=mark,indentWidth=indentWidth),
      startOfLine = true,
      mySpaces = 0,
      pre = UStream('\n'),
      out = UStream()
    )
  }

  private val spaces_ = mutable.Map[Int,UStream[Char]]()

  def spaces(len: Int): UStream[Char] = spaces_.synchronized {
    spaces_.getOrElseUpdate(
      len,
      UStream.repeat(' ').take(len)
    )
  }

  case class ForIndent(inner: Fmt) extends Fmt {
    override def format(state: State): Task[State] = {
      inner.format(state.copy(pre = state.pre ++ spaces(state.fixed.indentWidth)))
    }
  }

  case class ForChar(ch: Char) extends Fmt {
    override def format(state: State): Task[State] = Task {
      import state._
      ch match {
        case '\n' =>
            State(
              fixed = fixed,
              startOfLine = true,
              mySpaces = 0,
              pre = pre,
              out = out ++ pre
            )
        case ' ' if startOfLine =>
          State(
            fixed = fixed,
            startOfLine = startOfLine,
            mySpaces = mySpaces + 1,
            pre = pre,
            out = out
          )
        case _ if startOfLine && fixed.mark.contains(ch) =>
          State(
            fixed = fixed,
            startOfLine = startOfLine,
            mySpaces = 0,
            pre = pre,
            out = out
          )
        case _ =>
          val x = out ++ spaces(mySpaces)
          State(
            fixed = fixed,
            startOfLine = false,
            mySpaces = 0,
            pre = pre,
            out = x ++ UStream(ch)
          )

      }
    }
  }

  case class ForStream(stream: UStream[Fmt]) extends Fmt {
    override def format(initial_state: State): Task[State] = {
      val loop_state = State(
        fixed = initial_state.fixed,
        startOfLine = initial_state.startOfLine,
        mySpaces = 0,
        pre = initial_state.pre ++ spaces(initial_state.mySpaces),
        out = initial_state.out ++ spaces(initial_state.mySpaces)
      )
      val loop_output_task = stream.foldM(loop_state) { (step_input,f) =>
        f.format(step_input)
      }

      loop_output_task.map { loop_output =>
        State(
          fixed = loop_output.fixed,
          startOfLine = loop_output.startOfLine,
          mySpaces = loop_output.mySpaces,
          pre = initial_state.pre,
          out = loop_output.out
        )
      }
    }
  }

  def apply(x: Any): Fmt = x match {
    case c: Char => ForChar(c)
    case i: Int => apply(i.toString)
    case s: String => apply(s.toIterable)
    case f: Fmt => f
    case i: Iterable[_] => ForStream(UStream.fromIterable(i).map(apply))
    case s: ZStream[_,_,_] => ForStream(s.asInstanceOf[UStream[_]].map(apply))
    case _ => throw new Exception(s"type:${x.getClass.getName} value:$x")
  }

  def apply(x1: Any, x2: Any, xs: Any*): Fmt =
    apply(x1 +: x2 +: xs)

  def indent(x: Any): Fmt = ForIndent(apply(x))
}
