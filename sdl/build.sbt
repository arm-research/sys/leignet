lazy val sdl = (project in file(".")).settings(
  scalaVersion := "2.13.4",
  scalacOptions ++= Seq("-deprecation","-Xfatal-warnings","-Xlint"),
  mainClass in Compile := Some("sdl.main"),
  discoveredMainClasses in Compile := Seq(),
  libraryDependencies ++= Seq(
    // alkjfhslkdjfh "com.gheith" %% "fmt" % "0.1-SNAPSHOT",
    "dev.zio" %% "zio" % "1.0.4-2",
    "dev.zio" %% "zio-streams" % "1.0.4-2",
    "com.lihaoyi" %% "fastparse" % "2.3.1",
    "com.lihaoyi" %% "os-lib" % "0.7.1",
    "dev.zio" %% "zio-streams" % "1.0.4-2",
    "dev.zio" %% "zio" % "1.0.4-2",
    "org.scalactic" %% "scalactic" % "3.2.2",
    "org.scalatest" %% "scalatest" % "3.2.3" % Test,
    "org.scalatest" %% "scalatest-propspec" % "3.2.2" % Test
  )
).enablePlugins(JavaAppPackaging)


